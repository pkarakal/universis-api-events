# Universis Event Management

Universis api server scheduling services

##### Installation

    npm i @universis/events
    
 `@universis/events` plugin extends api server for managing events
 
 #### Schema Configuration
 
 Add EventSchemaLoader as schema loader
 
 server/config/app.production.json
 
     "schema": {
             ...
            "loaders": [
            {
              "loaderType": "@universis/events#EventSchemaLoader"
            }
            ]
             ...
         }

 ### Development

##### Clone universis-api server, install dependencies and build

        git clone https://gitlab.com/universis/universis-api.git
        cd universis-api
        npm i
        npm run build
        
Note: Following instructions provided at [https://gitlab.com/universis/universis-api/-/blob/master/INSTALL.md](https://gitlab.com/universis/universis-api/-/blob/master/INSTALL.md) for configuring universis-api server

#####  Clone project and install dependencies

       git clone https://gitlab.com/universis/universis-api-events.git
       cd universis-api-events
       npm i
       
##### Install universis-api as development dependency (for testing)

       npm i ../universis-api --save-dev --no-save
       
##### Test plugin

        npm test
 
 
###### Features

Event management system prototype models have the following features:

* Manage events of any type (organize parent events)
* Organize event based on event organizers like department etc
* Manage event status (e.g. cancel, postpone, schedule, re-schedule an event)
* Manage event performers, attendees and audiences
* Manage event locations
* Create event hours specification for recursive events
* Create and manage recursive events
* Manage person availability for performing or attending an event
* Testing api services
