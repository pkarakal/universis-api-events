import {EdmMapping} from '@themost/data/odata';
import {DataObject} from '@themost/data/data-object';

/**
 * @class
 
 * @property {number} about
 * @property {Audience|any} audience
 * @property {EventHoursSpecification|any} eventHoursSpecification
 * @property {number} remainingAttendeeCapacity
 * @property {Date} startDate
 * @property {Date} endDate
 * @property {Date} doorTime
 * @property {User} contributor
 * @property {number} maximumAttendeeCapacity
 * @property {Department} organizer
 * @property {Array<User>} attendees
 * @property {string} inLanguage
 * @property {EventStatusType|any} eventStatus
 * @property {Event} superEvent
 * @property {string} duration
 * @property {Date} previousStartDate
 * @property {Place|any} location
 * @property {boolean} isAccessibleForFree
 * @property {User} performer
 * @augments {DataObject}
 */
@EdmMapping.entityType('Event')
class Event extends DataObject {
    /**
     * @constructor
     */
    constructor() {
        super();
    }
}
module.exports = Event;
