import {EdmMapping} from '@themost/data/odata';
import {DataObject} from '@themost/data/data-object';

/**
 * @class
 
 * @property {number} id
 * @augments {DataObject}
 */
@EdmMapping.entityType('EventStatusType')
class EventStatusType extends DataObject {
    /**
     * @constructor
     */
    constructor() {
        super();
    }
}
module.exports = EventStatusType;
